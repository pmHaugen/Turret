# Turret

A gun turret schematics for Arduino Nano made in [wokwi](wokwi.com).

## Intro

This project simulates a gun turret made from servo moteors, LED light, a buzz speaker, buttons, LCD screen and a joystick. It consits of schematics and code for running on Arduino Nano.

## Contents

* [What is this?](#what-is-this)
* [Build](#build)
* [Usage](#usage)
* [Contribute](#contribute)

## What is this?

This project is schematics and code for making a fake gun turret (gun is lacking) from components for Arduino Nano.

### Build

To build this project you need 
* 1 Arduino Nano board
* 2 Servo motors
* 5 Buttons
* 1 LED light
* 1 Joystick
* 1 Buzz speaker
* 1 LCD screen

### Usage

Assemble the parts according to the [schematics](#Turret).

## Contribute

Contributors are Paal Marius Haugen and Thomas Haaland.

![Turret](turret.png)
