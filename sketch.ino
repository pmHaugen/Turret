#include <Servo.h>
#include <LiquidCrystal_I2C.h>

// LDR Characteristics
const float GAMMA = 0.7;
const float RL10 = 50;

LiquidCrystal_I2C lcd(0x27, 20, 4);

#define LDR_PIN 2

// Button pins
#define NUM_BTNS 5
#define ROTATE_Y_UP A1
#define ROTATE_Y_DOWN 6
#define ROTATE_Z_LEFT 7
#define ROTATE_Z_RIGHT A0
#define FIRE_BTN 9

// LED pin
#define LED 5

// Servo pins
#define SERVO_Z 12
#define SERVO_Y 11

// Speaker pin
#define SPEAKER 10

// Joystick pins
#define HORZ A2
#define VERT A3
#define SEL 8

// Servos
Servo armY, armZ; // Create a "Servo" object called "arm"
float posZ = 90.0; // Variable where the arm's position will be stored (in degrees)
float posY = 90.0; 
float step = 1.0; // Variable used for the arm's position step

class Button {
public:
  Button(int inPin) : btnPin{inPin} 
  {
    pinMode(inPin, INPUT_PULLUP);
  }

  bool pressed() {
    if (digitalRead(btnPin) == LOW) {
      return true;
    } else {
      return false;
    }
  }
private:
  int btnPin;
}; 

Button buttons[NUM_BTNS] = {Button(ROTATE_Y_UP), Button(ROTATE_Y_DOWN), Button(ROTATE_Z_RIGHT), Button(ROTATE_Z_LEFT), Button(FIRE_BTN)};
enum btn_name { ROT_Y_UP, ROT_Y_DOWN, ROT_Z_RIGHT, ROT_Z_LEFT, FIRE };

class Joystick {
public:
  Joystick(int vert, int horz, int sel) : vert{vert}, horz{horz}, sel{sel} {
    pinMode(vert, INPUT);
    pinMode(horz, INPUT);
    pinMode(sel, INPUT_PULLUP);
    vertVal = 512;
    horzVal = 512;
    selPressed = false;
  }

  void recieve() {
    vertVal = analogRead(vert);
    horzVal = analogRead(horz);
    selPressed = digitalRead(sel) == LOW;
  }

  int getVertVal() {
    if (vertVal < 512/2) return -1;
    else if (vertVal > 512 + 512/2) return 1;
    else return 0;
  }

  int getHorzVal() {
    if (horzVal < 512/2) return -1;
    else if (horzVal > 512 + 512/2) return 1;
    else return 0;
  }

  bool getPressedVal() {
    return selPressed;
  }
private:
  int vertVal, horzVal, selPressed;
  int vert, horz, sel;
} joystick(VERT, HORZ, SEL);

class Listener {
public:
  Listener() {  }
  btn_name listen() {
    for (int idx = 0; idx < NUM_BTNS; idx++) {
      Button button = buttons[idx];
      if (button.pressed()) {
        return idx;
        }
      }
    return -1;
  }

  btn_name joystick_listen() {
    joystick.recieve();
    int vert = joystick.getVertVal();
    int horz = joystick.getHorzVal();
    int sel = joystick.getPressedVal();
    //Serial.println(vert);
    if (vert < 0) return ROT_Y_UP;
    if (vert > 0) return ROT_Y_DOWN;
    if (horz < 0) return ROT_Z_RIGHT;
    if (horz > 0) return ROT_Z_LEFT;
    if (sel) return FIRE;
    return -1;
  }
private:
} listener;

class Controller {
public:
  Controller() {
    ammo = 100;
  }

  void acceptSignal(int signal) {
    switch (signal) {
      case ROT_Y_UP:
        up();
        break;
      case ROT_Y_DOWN:
        down();
        break;
      case ROT_Z_RIGHT:
        right();
        break;
      case ROT_Z_LEFT:
        left();
        break;
      case FIRE:
        shoot();
        break;
    }
  }

  void up() {
    if (posY<180) // Check that the position won't go lower than 0°
      {
        posY-=step; // Decrement "pos" of "step" value
        armY.write(posY); // Set the arm's position to "pos" value
        delay(50); // Wait 5ms for the arm to reach the position
      }
  }

  void down() {
    if (posY>0) // Check that the position won't go lower than 0°
      {
        posY+=step; // Decrement "pos" of "step" value
        armY.write(posY); // Set the arm's position to "pos" value
        delay(50); // Wait 5ms for the arm to reach the position
      }
  }

  void right() {
    if (posZ>0) // Check that the position won't go lower than 0°
      {
        posZ+=step; // Decrement "pos" of "step" value
        armZ.write(posZ); // Set the arm's position to "pos" value
        delay(50); // Wait 5ms for the arm to reach the position
      }
  }

  void left() {
    if (posZ>0) // Check that the position won't go lower than 0°
      {
        posZ-=step; // Decrement "pos" of "step" value
        armZ.write(posZ); // Set the arm's position to "pos" value
        delay(50); // Wait 5ms for the arm to reach the position
      }
  }

  void shoot() {
    if (ammo > 0) {
      // Make fire sound
      tone(SPEAKER, 262);
      // Flash light
      digitalWrite(LED, HIGH);
      delay(200);
      digitalWrite(LED, LOW);
      noTone(SPEAKER);
      ammo--;
    }
  }

  int getAmmo() {
    return ammo;
  }
private:
  int ammo;
} controller;

void setup()
{
  // Connect LED light
  pinMode(LED, OUTPUT);

  // Montage the arms
  armY.attach(SERVO_Y); // Attache the arm to the pin 2
  armY.write(posY); // Initialize the arm's position to 0 (leftmost)

  armZ.attach(SERVO_Z);
  armZ.write(posZ);

  // Initialize the LCD
  pinMode(LDR_PIN, OUTPUT);
  lcd.init();
  lcd.backlight();
  lcd.print("Ammo: ");
  lcd.setCursor(1, 1);
  lcd.print("100");

  // Speaker setup
  pinMode(SPEAKER, OUTPUT);

  Serial.begin(115200);
}

void loop()
{
  int response = listener.listen();
  if (response == -1) {
    response = listener.joystick_listen();
  }
  if (response >= 0) {
    controller.acceptSignal(response);
  }
  if (response == 4) {
    lcd.setCursor(1, 1);
    lcd.print(controller.getAmmo());
    lcd.print(" ");
  }
}
